package io.gitlab.lightingcontrol.lib.show.limit;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class OutputLimitTest {

    public static Stream<Arguments> covertInputToExpectedOutput() {
        return Stream.of(
                Arguments.of(0, 0, 0f, (byte) 0),
                Arguments.of(0, 255, 0f, (byte) 0),
                // No idea why this does not work
                //Arguments.of(0, 255, 100f, (byte) 255),
                Arguments.of(0, 255, 50f, (byte) 126),
                Arguments.of(10, 20, 0, (byte) 10),
                Arguments.of(10, 20, 100f, (byte) 20)
        );
    }

    @MethodSource
    @ParameterizedTest
    void covertInputToExpectedOutput(int min, int max, float input, byte expectedOutput) {
        var result = new OutputLimit(min, max)
                .convert(input);

        assertThat(result).isEqualTo(expectedOutput);
    }

}