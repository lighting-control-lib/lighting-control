package io.gitlab.lightingcontrol.lib.show.timer;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class EffectTimerTest {

    public static Stream<Arguments> effectTimerShouldOffsetCorrectValues() {
        return Stream.of(
                // At time zero and no offset the offset is always 0
                Arguments.of(1, (short) 120, 0, 0, 0f),
                // At first beat and no offset the offset locked is always 0
                Arguments.of(1, (short) 120, 60_000 / 120, 0, 0f),
                // At half beat and no offset the offset is half
                Arguments.of(1, (short) 120, 60_000 / 240, 0, 0.5f),
                // At half beat and no offset the offset is half
                Arguments.of(1, (short) 1, 30_000, 0, 0.5f),
                // At half beat and no offset the offset is half
                Arguments.of(1, (short) 1, 60_000 - 1, 0, 0.9999833f),
                // At half beat and no offset the offset is half
                Arguments.of(1, (short) 1, 60_000, 0, 0f),
                // Have 2 beats per cycle
                Arguments.of(2, (short) 120, 60_000 / 240, 0, 0.25f),
                Arguments.of(1, (short) 1, 60_000, 180, 0.5f)
        );
    }

    @ParameterizedTest
    @MethodSource
    void effectTimerShouldOffsetCorrectValues(float beatsPerCycle, short bpm, long millis, int offset, float expectedResult) {
        assertThat(
                new EffectTimer(offset, beatsPerCycle)
                        .getOffset(bpm, millis)
        ).isEqualTo(expectedResult);
    }

    @Test
    void effectTimerShouldNeverReturnNegativeOffset() {
        for (float beatsPerCycle = 0.25f; beatsPerCycle < 180; beatsPerCycle *= 2) {
            for (int offset = 0; offset < 360; offset += 18) {
                var sut = new EffectTimer(offset, beatsPerCycle);
                for (short bpm = 20; bpm < 180; bpm += 12) {
                    for (long millis = 0; millis < 60_000; millis += 12345) {
                        assertThat(sut.getOffset(bpm, millis))
                                .withFailMessage(
                                        "With %.3f beats per second and an offset of %d, EffectTimer returned a negative value for the bpm %d at millis of %d",
                                        beatsPerCycle, offset, bpm, millis)
                                .isNotNegative();
                    }
                }
            }
        }
    }
}