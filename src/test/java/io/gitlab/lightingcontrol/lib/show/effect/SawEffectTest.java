package io.gitlab.lightingcontrol.lib.show.effect;

import io.gitlab.lightingcontrol.configuration.CapabilityConfiguration;
import io.gitlab.lightingcontrol.configuration.SawEffectConfiguration;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class SawEffectTest {

    public static final CapabilityConfiguration CAPABILITY = new CapabilityConfiguration(0, 255);

    public static Stream<Arguments> calculationShouldBeCorrect() {
        return Stream.of(
                Arguments.of(0F, 100F, 0, 0),
                Arguments.of(0F, 100F, 0.5F, 50F),
                Arguments.of(0F, 100F, 1F, 100F),
                Arguments.of(100F, 0F, 0F, 100F),
                Arguments.of(100F, 0F, 0.5F, 50F),
                Arguments.of(100F, 0F, 1F, 0F),
                Arguments.of(0F, 0F, 0F, 0F),
                Arguments.of(0F, 0F, 0.5F, 0F),
                Arguments.of(0F, 0F, 1F, 0F),
                Arguments.of(10F, 20F, 0.5F, 15F)
        );
    }

    @MethodSource
    @ParameterizedTest
    void calculationShouldBeCorrect(float fromPercent, float toPercent, float offset, float expectedResult) {
        var sut = new SawEffect(new SawEffectConfiguration(fromPercent, toPercent), CAPABILITY, 0);

        var result = sut.calculate(offset);

        assertThat(result)
                .isEqualTo(expectedResult);
    }
}