package io.gitlab.lightingcontrol.lib.show.effect;

import io.gitlab.lightingcontrol.configuration.BlendEffectConfiguration;
import io.gitlab.lightingcontrol.configuration.CapabilityConfiguration;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class BlendEffectTest {

    public static final CapabilityConfiguration CAPABILITY = new CapabilityConfiguration(0, 255);

    public static Stream<Arguments> calculateShouldResult() {
        return Stream.of(
                Arguments.of(0, 100, 2, 0, 0f, 0f),
                Arguments.of(0, 100, 2, 0, 0.25f, 50f),
                Arguments.of(0, 100, 2, 0, 0.5f, 100f),
                Arguments.of(0, 100, 2, 0, 0.9999999999f, 0f),
                Arguments.of(0, 100, 4, 0, 0.1f, 0f),
                Arguments.of(0, 100, 4, 0, 0.2f, 0f),
                Arguments.of(0, 100, 4, 0, 0.25f, 0f),
                Arguments.of(0, 100, 4, 0, 0.5f, 100f),
                Arguments.of(0, 100, 4, 0, 0.75f, 0f),
                Arguments.of(0, 100, 4, 0, 0.8f, 0f),
                Arguments.of(0, 100, 4, 0, 0.9f, 0f)
        );
    }

    @ParameterizedTest
    @MethodSource
    void calculateShouldResult(float minPercentage, float maxPercentage, Integer channels, int effectOffset, float offset, float expectedResult) {

        var sut = new BlendEffect(
                new BlendEffectConfiguration(
                        minPercentage,
                        maxPercentage,
                        channels,
                        effectOffset,
                        1),
                CAPABILITY,
                0);

        var result = sut.calculate(offset);

        assertThat(result).isEqualTo(expectedResult);
    }
}