package io.gitlab.lightingcontrol.lib.show.effect;

import io.gitlab.lightingcontrol.configuration.CapabilityConfiguration;
import io.gitlab.lightingcontrol.configuration.StaticEffectConfiguration;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;

class StaticEffectTest {

    public static final CapabilityConfiguration CAPABILITY = new CapabilityConfiguration(0, 255);

    @ParameterizedTest
    @ValueSource(floats = {0, 0.1f, 0.2f, 0.4f, 0.5f, 0.6f, 0.7f, 0.8f, 0.8f, 1f})
    void calculateShouldResult(float percentage) {

        var sut = new StaticEffect(
                new StaticEffectConfiguration(percentage),
                CAPABILITY);

        assertThat(sut.calculate(percentage)).isEqualTo(percentage);
        assertThat(sut.calculate(1 - percentage)).isEqualTo(percentage);
    }
}