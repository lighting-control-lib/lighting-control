package io.gitlab.lightingcontrol.lib.show;

import io.gitlab.lightingcontrol.configuration.*;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

class ShowTest {
    public static void main(String[] args) throws InvalidConfigurationException {
        var counter = new AtomicInteger(1);
        var showConfiguration
                = new ShowConfiguration(
                "Example",
                Stream.of(
                        new MapEntry<>("red", new ChannelConfiguration(0, Map.of("color", new CapabilityConfiguration(0, 255)))),
                        new MapEntry<>("green", new ChannelConfiguration(0, Map.of("color", new CapabilityConfiguration(0, 255)))),
                        new MapEntry<>("blue", new ChannelConfiguration(0, Map.of("color", new CapabilityConfiguration(0, 255)))),
                        new MapEntry<>("amber", new ChannelConfiguration(0, Map.of("color", new CapabilityConfiguration(0, 255)))),
                        new MapEntry<>("uv", new ChannelConfiguration(0, Map.of("color", new CapabilityConfiguration(0, 255)))),
                        new MapEntry<>("master", new ChannelConfiguration(0, Map.of("dimmer", new CapabilityConfiguration(0, 255)))),
                        new MapEntry<>("speed", new ChannelConfiguration(0, Map.of("channel", new CapabilityConfiguration(0, 255)))),
                        new MapEntry<>("bar_mode", new ChannelConfiguration(0, Map.of(
                                "unused", new CapabilityConfiguration(0, 9),
                                "auto", new CapabilityConfiguration(10, 119),
                                "music", new CapabilityConfiguration(120, 255)
                        ))),
                        new MapEntry<>("bar_strobe", new ChannelConfiguration(0, Map.of(
                                "Flash off", new CapabilityConfiguration(0, 9),
                                "Flash", new CapabilityConfiguration(10, 255)
                        ))),
                        new MapEntry<>("flood_strobe", new ChannelConfiguration(0, Map.of(
                                "Flash off", new CapabilityConfiguration(0, 9),
                                "Flash", new CapabilityConfiguration(10, 255)
                        ))),
                        new MapEntry<>("thunder_sound", new ChannelConfiguration(0, Map.of(
                                "Sound off", new CapabilityConfiguration(0, 5),
                                "sound on", new CapabilityConfiguration(6, 255)
                        ))),
                        new MapEntry<>("thunder_strobe", new ChannelConfiguration(0, Map.of(
                                "Strobe open", new CapabilityConfiguration(0, 5),
                                "Strobe closed", new CapabilityConfiguration(6, 10),
                                "Pulse Random", new CapabilityConfiguration(11, 33),
                                "Ramp up Random", new CapabilityConfiguration(34, 56),
                                "Ramp down Random", new CapabilityConfiguration(57, 79),
                                "Random Strobe", new CapabilityConfiguration(80, 102),
                                "Strobe Break", new CapabilityConfiguration(103, 127),
                                "Strobe slow", new CapabilityConfiguration(128, 250),
                                "Strobe open (2)", new CapabilityConfiguration(251, 255)
                        ))),
                        new MapEntry<>("par_strobe", new ChannelConfiguration(255, Map.of(
                                "LED off", new CapabilityConfiguration(0, 31),
                                "LED on (1)", new CapabilityConfiguration(32, 63),
                                "Strobe", new CapabilityConfiguration(64, 95),
                                "LED on (2)", new CapabilityConfiguration(96, 127),
                                "Pulse", new CapabilityConfiguration(128, 159),
                                "LED on (3)", new CapabilityConfiguration(160, 191),
                                "Random Strobe", new CapabilityConfiguration(192, 223),
                                "LED on (4)", new CapabilityConfiguration(224, 255)
                        ))),
                        new MapEntry<>("hellball_mode", new ChannelConfiguration(0, Map.of(
                                "Mode DMX", new CapabilityConfiguration(0, 5),
                                "Mode Built-in", new CapabilityConfiguration(6, 127),
                                "Mode Sound", new CapabilityConfiguration(128, 255)
                        ))),
                        new MapEntry<>("hellball_rotation", new ChannelConfiguration(0, Map.of(
                                "Rotation off", new CapabilityConfiguration(0, 5),
                                "Rotation fixed", new CapabilityConfiguration(6, 127),
                                "Rotation on", new CapabilityConfiguration(128, 255)
                        ))),
                        new MapEntry<>("hellball_strobe", new ChannelConfiguration(0, Map.of(
                                "Strobe off", new CapabilityConfiguration(0, 5),
                                "Strobe on", new CapabilityConfiguration(6, 255)
                        ))),
                        new MapEntry<>("unused", new ChannelConfiguration(0, Map.of(
                                "value", new CapabilityConfiguration(0, 255)
                        )))
                ).collect(Collectors.toMap(MapEntry::name, MapEntry::object)),
                Map.of(
                        "par_36_5", new FixtureConfiguration(List.of("unused", "red", "green", "blue", "unused")),
                        "par_6", new FixtureConfiguration(List.of("red", "green", "blue", "uv", "par_strobe", "master")),
                        "flood_8", new FixtureConfiguration(List.of("master", "red", "green", "blue", "flood_strobe", "unused", "unused", "unused")),
                        "thunder_6", new FixtureConfiguration(List.of("master", "thunder_strobe", "red", "green", "blue", "thunder_sound")),
                        "rgb", new FixtureConfiguration(List.of("red", "green", "blue")),
                        "bar_9", new FixtureConfiguration(List.of("red", "green", "blue", "amber", "unused", "master", "bar_strobe", "unused", "speed")),
                        "pixel_24", new FixtureConfiguration(List.of(
                                "red", "green", "blue",
                                "red", "green", "blue",
                                "red", "green", "blue",
                                "red", "green", "blue",
                                "red", "green", "blue",
                                "red", "green", "blue",
                                "red", "green", "blue",
                                "red", "green", "blue"
                        )),
                        "hellball_6", new FixtureConfiguration(List.of("hellball_strobe", "red", "green", "blue", "hellball_rotation", "hellball_mode"))
                ),
                Stream.of(
                        new MapEntry<>("bpm", new DeviceConfiguration(counter.getAndAdd(5), "par_36_5", false)),
                        new MapEntry<>("par_1", new DeviceConfiguration(counter.getAndAdd(6), "par_6", false)),
                        new MapEntry<>("par_2", new DeviceConfiguration(counter.getAndAdd(6), "par_6", false)),
                        new MapEntry<>("par_3", new DeviceConfiguration(counter.getAndAdd(6), "par_6", false)),
                        new MapEntry<>("par_4", new DeviceConfiguration(counter.getAndAdd(6), "par_6", false)),
                        new MapEntry<>("flood_1", new DeviceConfiguration(counter.getAndAdd(8), "flood_8", false)),
                        new MapEntry<>("flood_2", new DeviceConfiguration(counter.getAndAdd(8), "flood_8", false)),
                        new MapEntry<>("thunder_1", new DeviceConfiguration(counter.getAndAdd(6), "thunder_6", false)),
                        new MapEntry<>("thunder_2", new DeviceConfiguration(counter.getAndAdd(6), "thunder_6", false)),
                        new MapEntry<>("bar_1", new DeviceConfiguration(counter.getAndAdd(9), "bar_9", false)),
                        new MapEntry<>("bar_2", new DeviceConfiguration(counter.getAndAdd(9), "bar_9", false)),
                        new MapEntry<>("pixel_1", new DeviceConfiguration(counter.get(), "pixel_24", false)),
                        new MapEntry<>("pixel_1a", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_1b", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_1c", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_1d", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_2", new DeviceConfiguration(counter.get(), "pixel_24", false)),
                        new MapEntry<>("pixel_2a", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_2b", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_2c", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_2d", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_3", new DeviceConfiguration(counter.get(), "pixel_24", false)),
                        new MapEntry<>("pixel_3a", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_3b", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_3c", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_3d", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_4", new DeviceConfiguration(counter.get(), "pixel_24", false)),
                        new MapEntry<>("pixel_4a", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_4b", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_4c", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("pixel_4d", new DeviceConfiguration(counter.getAndAdd(3), "rgb", true)),
                        new MapEntry<>("hellball", new DeviceConfiguration(counter.getAndAdd(6), "hellball_6", true))
                ).collect(toMap(MapEntry::name, MapEntry::object)),
                Map.of(
                        "blackout", Set.of(
                                new SceneConfiguration(
                                        List.of(
                                                "bpm"),
                                        Set.of(
                                                new ChannelCapabilityConfiguration("red", "color"),
                                                new ChannelCapabilityConfiguration("green", "color"),
                                                new ChannelCapabilityConfiguration("blue", "color"),
                                                new ChannelCapabilityConfiguration("master", "dimmer")),
                                        new BlendEffectConfiguration(0, 100, 2), 1),
                                // new SawEffectConfiguration(0, 100), 0),
                                new SceneConfiguration(
                                        List.of(
                                                // "bpm",
                                                "par_1", "par_2", "par_3", "par_4",
                                                "flood_1", "flood_2",
                                                "thunder_1", "thunder_2",
                                                "bar_1", "bar_2",
                                                "pixel_1", "pixel_2", "pixel_3", "pixel_4",
                                                "hellball"),
                                        Set.of(
                                                new ChannelCapabilityConfiguration("red", "color"),
                                                new ChannelCapabilityConfiguration("green", "color"),
                                                new ChannelCapabilityConfiguration("blue", "color"),
                                                new ChannelCapabilityConfiguration("master", "dimmer")),
                                        new StaticEffectConfiguration(10), 1)
                        )
                ),
                List.of(new StepConfiguration("Blackout", 120, Set.of("blackout")))
        );
        var show = new ShowFactory().build(showConfiguration);
        System.out.println(show);
        long start = System.nanoTime();
        byte[] result = show.dmxValues(0);
        long end = System.nanoTime();
        System.out.println(Arrays.toString(result));
        System.out.println("Calculated " + (end - start) + " nanos");
        for (long i = 0; i < 510; i += 5) {
            result = show.dmxValues(i);
            // System.out.println(Arrays.toString(result));
        }
    }
}

record MapEntry<O>(String name, O object) {
}
