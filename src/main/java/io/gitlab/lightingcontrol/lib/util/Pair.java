package io.gitlab.lightingcontrol.lib.util;

record Pair<E>(String key, E object) {
}
