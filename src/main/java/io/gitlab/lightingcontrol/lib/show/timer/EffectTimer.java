package io.gitlab.lightingcontrol.lib.show.timer;

import lombok.ToString;

@ToString
public class EffectTimer {

    private final float beatsPerCycle;
    private final float offset;

    /**
     * @param beatsPerCycle between 0.25 and 256
     */
    public EffectTimer(float beatsPerCycle) {
        this((short) 0, beatsPerCycle);
    }

    /**
     * @param beatsPerCycle between 0.25 and 256
     * @param offset        between 0 and 359
     */
    public EffectTimer(int offset, float beatsPerCycle) {
        if (offset < 0 || offset >= 360) {
            throw new RuntimeException("Offset needs to be from 0 (including) to 360 (excluding)");
        }
        this.beatsPerCycle = beatsPerCycle;
        this.offset = (float) offset / 360;
    }

    public float getOffset(int bpm, long timeMillis) {
        float result = 0;

        if (bpm != 0 && beatsPerCycle != 0) {
            float millisPerCycle = (60_000 * beatsPerCycle) / bpm;
            long offsetedMillis = timeMillis + (long) (offset * millisPerCycle);
            long offsetInCycle = offsetedMillis % (long) millisPerCycle;
            result = offsetInCycle / millisPerCycle;
        }

        return result;
    }

}
