package io.gitlab.lightingcontrol.lib.show;

public interface ShowUpdateListener {

    public void showUpdated(ShowUpdate update);

}
