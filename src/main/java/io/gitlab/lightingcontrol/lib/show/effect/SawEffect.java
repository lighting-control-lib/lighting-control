package io.gitlab.lightingcontrol.lib.show.effect;

import io.gitlab.lightingcontrol.configuration.CapabilityConfiguration;
import io.gitlab.lightingcontrol.configuration.SawEffectConfiguration;
import lombok.ToString;

@ToString(callSuper = true)
public class SawEffect extends Effect {

    private final float from;
    private final float to;

    protected SawEffect(SawEffectConfiguration configuration, CapabilityConfiguration capability, int spreadOffset) {
        super(capability, (configuration.getOffset() + spreadOffset) % 360, configuration.getBeatsPerCycle());
        from = configuration.getFromPercent();
        to = configuration.getToPercent();
    }

    @Override
    protected float calculate(float offset) {
        return (to - from) * offset + from;
    }

}
