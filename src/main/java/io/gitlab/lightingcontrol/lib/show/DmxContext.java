package io.gitlab.lightingcontrol.lib.show;

public record DmxContext(int bpm, long instant) {
}
