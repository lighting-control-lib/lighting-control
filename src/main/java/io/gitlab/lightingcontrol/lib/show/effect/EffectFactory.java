package io.gitlab.lightingcontrol.lib.show.effect;

import io.gitlab.lightingcontrol.configuration.*;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class EffectFactory {

    private final Map<EffectConfiguration, Effect> cache;

    public EffectFactory() {
        this.cache = new HashMap<>();
    }

    public Effect build(EffectConfiguration configuration, CapabilityConfiguration capability, int spreadOffset) {
        return switch (configuration) {
            case StaticEffectConfiguration s -> getEffect(s, () -> new StaticEffect(s, capability));
            case SawEffectConfiguration s -> getEffect(s, () -> new SawEffect(s, capability, spreadOffset));
            case BlendEffectConfiguration s -> getEffect(s, () -> new BlendEffect(s, capability, spreadOffset));

            default -> throw new IllegalStateException("Unexpected value: " + configuration);
        };
    }

    private Effect getEffect(EffectConfiguration s, Supplier<Effect> effectConfigurationEffectFunction) {
        return cache.computeIfAbsent(s, ec -> effectConfigurationEffectFunction.get());
    }

}
