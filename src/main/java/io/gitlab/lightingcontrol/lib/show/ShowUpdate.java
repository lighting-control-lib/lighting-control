package io.gitlab.lightingcontrol.lib.show;

public record ShowUpdate(String title, String stepName, int bpm, String previousStepName, String nextStepname) {
}
