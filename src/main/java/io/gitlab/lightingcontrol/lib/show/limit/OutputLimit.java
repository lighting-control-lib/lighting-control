package io.gitlab.lightingcontrol.lib.show.limit;

import lombok.ToString;

@ToString
public class OutputLimit {

    private final int min;
    private final int max;

    public OutputLimit(int min, int max) {
        if (min < 0 || min > 255 || max < 0 || max > 255) {
            throw new RuntimeException("Limit need to provide values between 0 and 255");
        }

        this.min = Math.min(min, max);
        this.max = Math.max(min, max);
    }

    public byte convert(float input) {
        return (byte) (Math.floor((max - min + 1) * input / (101f)) + min);
    }
}
