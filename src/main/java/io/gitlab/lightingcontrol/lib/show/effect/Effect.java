package io.gitlab.lightingcontrol.lib.show.effect;

import io.gitlab.lightingcontrol.configuration.CapabilityConfiguration;
import io.gitlab.lightingcontrol.lib.show.DmxContext;
import io.gitlab.lightingcontrol.lib.show.limit.OutputLimit;
import io.gitlab.lightingcontrol.lib.show.timer.EffectTimer;
import lombok.ToString;

@ToString
public abstract class Effect {

    private final EffectTimer effectTimer;
    private final OutputLimit outputLimit;

    protected Effect(CapabilityConfiguration capability, int offset, float beatsPerCycle) {
        this.effectTimer = new EffectTimer(offset, beatsPerCycle);
        this.outputLimit = new OutputLimit(capability.minValue(), capability.maxValue());
    }

    public byte calculate(DmxContext dmxContext) {
        float offset = this.effectTimer.getOffset(dmxContext.bpm(), dmxContext.instant());
        return convert(calculate(offset));
    }

    private byte convert(float offset) {
        return outputLimit.convert(offset);
    }

    protected abstract float calculate(float offset);

}
