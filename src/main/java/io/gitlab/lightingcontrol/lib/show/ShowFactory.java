package io.gitlab.lightingcontrol.lib.show;

import io.gitlab.lightingcontrol.configuration.*;
import io.gitlab.lightingcontrol.lib.show.effect.Effect;
import io.gitlab.lightingcontrol.lib.show.effect.EffectFactory;

import java.util.*;

public class ShowFactory {

    private final EffectFactory effectFactory;

    public ShowFactory() {
        effectFactory = new EffectFactory();
    }

    public Show build(ShowConfiguration showConfiguration) throws InvalidConfigurationException {
        return new Show(
                showConfiguration.title(),
                getDefaults(showConfiguration),
                getSteps(showConfiguration));
    }

    private List<Step> getSteps(ShowConfiguration showConfiguration) throws InvalidConfigurationException {
        List<Step> result = new ArrayList<>();
        for (var stepConfiguration : showConfiguration.steps()) {
            var effects = getEffects(stepConfiguration, showConfiguration);
            var step = new Step(stepConfiguration.name(), stepConfiguration.bpm(), effects);
            result.add(step);
        }
        return result;
    }

    private Map<Integer, Effect> getEffects(
            StepConfiguration stepConfiguration,
            ShowConfiguration showConfiguration) throws InvalidConfigurationException {
        var result = new HashMap<Integer, Effect>();
        for (var sceneName : stepConfiguration.scenes()) {
            for (var sceneConfiguration : getScene(sceneName, showConfiguration.scenes())) {
                var sceneDevice = 0;
                for (var deviceName : sceneConfiguration.devices()) {
                    var device = getDevice(deviceName, showConfiguration.devices());
                    var fixture = getFixture(device.fixture(), showConfiguration.fixtures());
                    var deviceUsed = false;
                    for (var channelCapability : sceneConfiguration.capabilities()) {
                        for (int i = 0; i < fixture.channels().size(); i++) {
                            if (channelCapability.channel().equals(fixture.channels().get(i))) {
                                var channel = getChannel(channelCapability.channel(), showConfiguration.channels());
                                if (channel.capabilities().containsKey(channelCapability.capability())) {
                                    var capability = channel.capabilities().get(channelCapability.capability());
                                    var configuration = sceneConfiguration.effectConfiguration();
                                    result.put(
                                            device.startAddress() + i,
                                            effectFactory.build(
                                                    configuration,
                                                    capability,
                                                    sceneConfiguration.spread() * sceneDevice));
                                    deviceUsed = true;
                                }
                            }
                        }
                    }
                    if (deviceUsed) {
                        sceneDevice += 1;
                    }
                }
            }
        }
        return result;
    }

    private FixtureConfiguration getFixture(String fixtureName, Map<String, FixtureConfiguration> fixtures) throws InvalidConfigurationException {
        if (!fixtures.containsKey(fixtureName)) {
            throw new InvalidConfigurationException("Fixture " + fixtureName + " is not defined");
        } else
            return fixtures.get(fixtureName);
    }

    private Map<Integer, Byte> getDefaults(ShowConfiguration showConfiguration) throws InvalidConfigurationException {
        var result = new HashMap<Integer, Byte>();

        for (DeviceConfiguration d : showConfiguration.devices().values()) {
            var address = d.startAddress();
            for (var channel : showConfiguration.fixtures().get(d.fixture()).channels()) {
                result.put(address, (byte) getChannel(channel, showConfiguration.channels()).defaultValue());
                address += 1;
            }
        }

        return result;
    }

    private DeviceConfiguration getDevice(
            String deviceName,
            Map<String, DeviceConfiguration> devices) throws InvalidConfigurationException {
        if (!devices.containsKey(deviceName)) {
            throw new InvalidConfigurationException("Device " + deviceName + " is not defined");
        } else
            return devices.get(deviceName);
    }

    private ChannelConfiguration getChannel(
            String channelName,
            Map<String, ChannelConfiguration> channels) throws InvalidConfigurationException {
        if (!channels.containsKey(channelName)) {
            throw new InvalidConfigurationException("Channel " + channelName + " is not defined");
        } else
            return channels.get(channelName);
    }

    private Set<SceneConfiguration> getScene(
            String sceneName,
            Map<String, Set<SceneConfiguration>> scenes) throws InvalidConfigurationException {
        if (!scenes.containsKey(sceneName)) {
            throw new InvalidConfigurationException("Scene " + sceneName + " is not defined");
        } else
            return scenes.get(sceneName);
    }
}
