package io.gitlab.lightingcontrol.lib.show;

import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

@ToString
public class Show {

    @Getter
    private final String title;
    private final Map<Integer, Byte> defaults;
    private final List<Step> steps;
    private final AtomicInteger step;
    @Getter
    private final Integer minChannel;
    @Getter
    private final Integer maxChannel;
    private final Set<ShowUpdateListener> listener;

    public Show(
            String title,
            Map<Integer, Byte> defaults,
            List<Step> steps
    ) {
        this.title = title;
        this.defaults = defaults;
        this.steps = steps;
        this.minChannel = this.defaults.keySet().stream().min(Integer::compareTo).orElseThrow();
        this.maxChannel = this.defaults.keySet().stream().max(Integer::compareTo).orElseThrow();
        this.step = new AtomicInteger(0);
        this.listener = new HashSet<>();
    }

    public byte[] dmxValues(long timeMillis) {
        var nowStep = steps.get(step.get());
        var context = new DmxContext(nowStep.bpm, timeMillis);
        var result = new byte[maxChannel - minChannel + 1];
        for (int i = 1; i < maxChannel; i++) {
            result[i] = nowStep.channels.containsKey(i)
                    ? (byte) nowStep.channels.get(i).calculate(context)
                    : (byte) defaults.get(i);
        }
        return result;
    }

    public int getStep() {
        return step.get();
    }

    public String getStepName() {
        return this.steps.get(step.get()).getName();
    }

    public int getBpm() {
        return this.steps.get(step.get()).getBpm();
    }

    public void addListener(@NonNull ShowUpdateListener listener) {
        this.listener.add(listener);
        listener.showUpdated(currentState());
    }

    public void removeListener(@NonNull ShowUpdateListener listener) {
        this.listener.remove(listener);
    }

    public synchronized ShowUpdate nextStep() {
        if (getStep() < steps.size() - 1) {
            step.incrementAndGet();
        }

        updateListeners();
        return currentState();
    }

    public synchronized ShowUpdate previousStep() {
        if (getStep() > 0) {
            step.decrementAndGet();
        }

        updateListeners();
        return currentState();
    }

    private void updateListeners() {
        var state = currentState();
        this.listener.forEach(l -> l.showUpdated(state));
    }

    private ShowUpdate currentState() {
        var currentStep = step.get();
        return new ShowUpdate(
                getTitle(),
                getStepName(),
                getBpm(),
                currentStep > 0 ? steps.get(currentStep - 1).getName() : null,
                currentStep < steps.size() - 1 ? steps.get(currentStep + 1).getName() : null);
    }

}