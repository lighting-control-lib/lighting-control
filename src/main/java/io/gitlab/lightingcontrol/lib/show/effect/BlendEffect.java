package io.gitlab.lightingcontrol.lib.show.effect;

import io.gitlab.lightingcontrol.configuration.BlendEffectConfiguration;
import io.gitlab.lightingcontrol.configuration.CapabilityConfiguration;
import lombok.ToString;

@ToString(callSuper = true)
public class BlendEffect extends Effect {

    private final float min;
    private final float max;
    private final int channels;

    protected BlendEffect(BlendEffectConfiguration configuration, CapabilityConfiguration capability, int spreadOffset) {
        super(capability, (configuration.getOffset() + spreadOffset) % 360, configuration.getBeatsPerCycle());
        min = Math.min(configuration.getMinPercentage(), configuration.getMaxPercentage());
        max = Math.max(configuration.getMaxPercentage(), configuration.getMinPercentage());
        channels = configuration.getChannels();
    }

    @Override
    protected float calculate(float offset) {
        double b = 1 - ((Math.abs(offset - 0.5) * channels));
        return (float) Math.max(0, b) * (max - min) + min;
    }

}
