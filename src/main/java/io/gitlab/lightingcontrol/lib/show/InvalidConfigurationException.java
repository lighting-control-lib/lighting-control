package io.gitlab.lightingcontrol.lib.show;

public class InvalidConfigurationException extends Throwable {

    InvalidConfigurationException(String message) {
        super(message);
    }
}
