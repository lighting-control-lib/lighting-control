package io.gitlab.lightingcontrol.lib.show.effect;


import io.gitlab.lightingcontrol.configuration.CapabilityConfiguration;
import io.gitlab.lightingcontrol.configuration.StaticEffectConfiguration;
import lombok.ToString;

@ToString(callSuper = true)
public class StaticEffect extends Effect {

    private final float percentage;

    protected StaticEffect(StaticEffectConfiguration configuration, CapabilityConfiguration capability) {
        super(capability, 0, 0);
        percentage = configuration.getPercentage();
    }

    @Override
    protected float calculate(float offset) {
        return percentage;
    }

}
