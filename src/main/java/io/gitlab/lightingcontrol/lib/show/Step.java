package io.gitlab.lightingcontrol.lib.show;

import io.gitlab.lightingcontrol.lib.show.effect.Effect;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;

@AllArgsConstructor
@Data
public class Step {

    String name;
    int bpm;
    Map<Integer, Effect> channels;

}
